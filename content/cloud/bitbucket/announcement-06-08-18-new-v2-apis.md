---
title: "New Bitbucket Cloud 2.0 APIs"
platform: cloud
product: bitbucketcloud
category: devguide
subcategory: updates
date: "2018-06-12"
---

# New Bitbucket Cloud 2.0 APIs

We are happy to announce an update to the Bitbucket Cloud 2.0 REST API, designed to offer developers a more robust and consistent experience building Bitbucket Cloud integrations. And while we've improved the API and its documentation to make for a smoother integration experience, we're most excited for you to try the changes we've made to Bitbucket Connect and the API Proxy. It's now easier than ever to build efficient and performant apps for Bitbucket Cloud.

## Filtering results with BBQL and partial responses

[Bitbucket Query Language (BBQL)](/bitbucket/api/2/reference/meta/filtering) is a generic querying language you can use to filter results from Bitbucket. With BBQL you can configure your Bitbucket integrations to request and handle only the specific data that matters to you. Different but related is the ability to query for [partial responses](/bitbucket/api/2/reference/meta/partial-response), since this lets you be explicit about what fields you do or don't want included in the response. These methods for filtering response data aren't only useful for trimming down the data returned to your application, they also improve the time taken to process the request in Bitbucket thanks to lazy evaluation of the data being returned. If you're familiar with GraphQL, you'll find that the combination of BBQL and partial responses brings a lot of that power and flexibility to plain REST endpoints.

## Improvements to our developer documentation

Good APIs need good documentation, so we spent time addressing the gaps and inconsistencies we found in our developer documentation with this update. Our [new API documentation](/bitbucket/api/2/reference/) is built on top of the [Open API Specification 2.0](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md) (formerly [Swagger](https://swagger.io/)), and as an Open API member organization, we built and released the [RADAR doc generator tool](https://developer.atlassian.com/blog/2016/05/open-api-initiative/) for rendering documentation written according to this specification. Our updated documentation offers a more thorough and understandable overview of what endpoints are available and how they work.

![Bitbucket Cloud new documentation](/cloud/bitbucket/images/screenshot-radar.png)

Check out the new documentation for:

- Querying [user](/bitbucket/api/2/reference/resource/user/permissions) and [team](/bitbucket/api/2/reference/resource/teams/%7Busername%7D/permissions) permissions.
- [Creating](/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/issues/%7Bissue_id%7D#put) and [querying](/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/issues/%7Bissue_id%7D/changes) issue updates.
- [Creating forks](/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/forks#post).
- [Commit history of a file](/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/filehistory/%7Bnode%7D/%7Bpath%7D).
- [Querying SSH keys](/bitbucket/api/2/reference/resource/users/%7Busername%7D/ssh-keys).
- A new and improved [diffstat API](/bitbucket/api/2/reference/search?q=diffstat).
- Updating [issue comments](/bitbucket/api/2/reference/search?q=issues+comments) and [pull request comments](/bitbucket/api/2/reference/search?q=pullrequests+comments).
- Creating and deleting [branches](/bitbucket/api/2/reference/search?q=branches) and [tags](/bitbucket/api/2/reference/search?q=tags).
- Creating and retrieving [commit comments](/bitbucket/api/2/reference/search?q=commit+comments).

## The API proxy module

New to Bitbucket Cloud, the [API proxy module](/cloud/bitbucket/modules/api-proxy/) creates a layer between your API and Bitbucket to provide the heavy lifting for critical services like authentication and parameter substitution (among others). Another huge benefit of using the proxy module is taking advantage of [response object inflation](/cloud/bitbucket/proxy-object-inflation/), which allows remote services to keep track of only the absolute minimum information to uniquely identify an object stored in Bitbucket. The API proxy module should make the API integrations and apps you build into Bitbucket Cloud more efficient and easier to build. We've also published an [API proxy module guide](/cloud/bitbucket/proxy-module/) to help get you started using the proxy module. Be sure to check it out.

The API proxy module will make it easier and more efficient to build apps and API integrations into Bitbucket Cloud.

## Give us feedback

Like everything we build at Atlassian, we depend on feedback from our integrations partners. Tell us what's working, what's missing, or where we can improve by leaving us some feedback in the [Atlassian Developer Community](https://community.developer.atlassian.com/tags/c/bitbucket-development/bitbucket-cloud/rest-api).