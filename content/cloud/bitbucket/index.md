---
title: "Latest updates"
platform: cloud
product: bitbucketcloud
category: devguide
subcategory: index
date: "2018-06-12"
---

# Latest updates

We deploy updates to Bitbucket Cloud frequently. As a Bitbucket developer, it's important that you're aware of the changes. The resources below will help you keep track of what's happening.

{{% warning title="Bitbucket Cloud 1.0 APIs are deprecated" %}}

Bitbucket Cloud REST API version 1 is deprecated effective 30 June 2018. All `1.0` APIs will be removed from the REST API permanently on 31 December 2018. Read the [deprecation notice](/cloud/bitbucket/deprecation-notice-v1-apis/).

{{% /warning %}}

## Recent announcements

Changes announced in the Atlassian Developer blog are usually described in more detail in this documentation. The most recent announcements are documented in detail below:

- [Deprecation notice - Bitbucket Cloud REST API version 1 is deprecated effective 30 June 2018](/cloud/bitbucket/deprecation-notice-v1-apis/)
- [Announcement - New 2.0 APIs recently released!](/cloud/bitbucket/announcement-06-08-18-new-v2-apis/)

## Atlassian Developer blog

In the **Atlassian Developer blog** you'll find handy tips and articles related to Bitbucket development.

Check it out and subscribe here: [Atlassian Developer blog](https://developer.atlassian.com/blog/categories/bitbucket/) *(Bitbucket-related posts)*.

## Bitbucket blog

Major changes that affect all users of the Bitbucket Cloud products are announced in the **Bitbucket blog**. This includes new features, bug fixes, and other changes.

Check it out and subscribe here: [Bitbucket blog](https://blog.bitbucket.org/).