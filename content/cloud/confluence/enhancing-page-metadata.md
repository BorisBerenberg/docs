---
title: "Enhancing page metadata"
platform: cloud
product: confcloud
category: devguide
subcategory: learning
patterns: patterns
date: "2017-08-28"
aliases:
- confcloud/enhancing-page-metadata-39988127.html
- /confcloud/enhancing-page-metadata-39988127.md
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988127
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988127
confluence_id: 39988127
---

# Enhancing page metadata

## Graphic elements required

-   16x16 brand or app icon to be displayed in the byline along with the text.

## How do users find metadata?

The byline extension lets apps provide users with further information about a page's metadata.

## How should the interaction work?

-   Users can find basic metadata in the byline, below the page title.
-   If they then click on the app icon or insight text, an inline dialog will show further information or actions related to the app.
-   The **View more... ** button lets apps take the user to a dedicated view to see additional details or complete related actions.

## UI components

[Web items](/cloud/confluence/modules/web-item/), inline dialogs, and modal dialogs can be used to build byline extensions.

![byline extension](/cloud/confluence/images/byline-extension.png)

##  Recommendation

-   Use progressive disclosure in surfacing the metadata. 
