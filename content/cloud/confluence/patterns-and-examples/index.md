---
Title: "Patterns and examples"
product: confcloud
category: devguide
---

# Patterns and examples

## Blueprints

[Blueprints](/cloud/confluence/blueprints)

Create pages based on pre-defined content, using readily available blueprints or your own custom-made blueprints.

## Custom content

[Custom content](/cloud/confluence/custom-content)

Create your own custom content, such as pages, blogs, and macro attachments.

[Display custom content in search](/cloud/confluence/display-custom-content-in-search)

Make custom content easily discoverable via Confluence's quick search and full search options.

[Display custom content in attachments](/cloud/confluence/display-custom-content-in-attachments)

Make custom content within attachments easily discoverable using consistent graphic elements and customized UI components.

## Macros

[Macros](/cloud/confluence/macros)

Create macros, which provide dynamic content inside Confluence pages, and insert them into any content you edit.

[Adding macro content to a page](/cloud/confluence/adding-macro-content-to-a-page)

Add your custom or pre-defined macro to the Insert menu, Select Macro dialog, or Macro placeholder.

[Viewing custom macro content in an overlay](/cloud/confluence/viewing-custom-macro-content-in-an-overlay)

View macro content in detail and design a consistent full-screen overlay for custom macro content. 

[Editing macro content in full screen](/cloud/confluence/editing-macro-content-in-full-screen)

Use the full screen macro dialog to bring your existing SaaS product's user interface right into Confluence. 

[Editing macro properties](/cloud/confluence/editing-macro-properties)

Use consistent graphic elements and customized UI components to enable efficient editing of your macro's properties.

[Interacting with macros in view mode](/cloud/confluence/interacting-with-macros-in-view-mode)

Customize the view mode toolbar so that users can use surface key features to interact with app content.

## Page extensions

[Page extensions](/cloud/confluence/page-extensions)

Allow your app to define context and actions, like implementing a custom workflow, that add value to content for users.

[Enhancing page metadata](/cloud/confluence/enhancing-page-metadata)

Configure apps to provide users with further information about a page’s metadata with byline extensions.

[Extending page actions](/cloud/confluence/extending-page-actions)

Group and store additional page actions in the More Actions menu of any Confluence page.

## Patterns

[Confluence Connect patterns](/cloud/confluence/confluence-connect-patterns/)

Confluence Connect patterns are common ways for you to extend the functionality of Confluence Cloud.

## REST API

[REST API examples](/cloud/confluence/rest-api-examples/)

Confluence REST API examples for common use cases.

## Search extensions

[Search extensions](/cloud/confluence/search-extensions)

Assign custom properties to Confluence content and enable search filtering using those properties with search extensions.


## Theming

[Theming](/cloud/confluence/theming)

Change the look and feel of headers, spaces, pages, and blog posts in Confluence Cloud with Confluence Connect themes.

[Theme properties](/cloud/confluence/theme-properties)

Change the look and feel of pages, blog posts, and the Confluence header by applying `lookAndFeel` properties to your Confluence theme.

## Examples

[Sequence Diagramr](https://bitbucket.org/atlassianlabs/atlassian-connect-confluence-sequence-diagramr) (node.js)

A Confluence macro for creating UML sequence diagrams on the page.

[Tim's Word Cloud](https://bitbucket.org/atlassianlabs/atlassian-connect-confluence-word-cloud) (node.js)

A macro that takes the contents of a page and constructs an SVG based word cloud.

[Google Maps macro](https://bitbucket.org/acgmaps/acgmaps.bitbucket.org) (static)

Insert Google Maps into your Confluence pages.

[Sketchfab for Confluence](https://bitbucket.org/robertmassaioli/sketchfab-connect) (static)

A static Atlassian Connect app that renders Sketchfab 3D models in Confluence pages.