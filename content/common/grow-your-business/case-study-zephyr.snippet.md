<h2 class="title">Zephyr</h2>

Zephyr wanted its app to sit inside of Jira to deliver the best functionality to customers. At AtlasCamp, Zephyr developers worked side-by-side with Atlassian to build the integration. Samir Shah, founder and CEO at Zephyr, said, “Atlassian was so open about everything, helping us through the process. They asked what we needed and provided APIs and source code. No other company will do that for you.”

<a href="/showcase/zephyr/">Learn more<i class="fa fa-arrow-right" aria-hidden="true"></i></a>